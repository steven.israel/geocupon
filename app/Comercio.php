<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'comercio';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcomercio';

    /**
     * @var array
     */
    protected $fillable = ['titulo', 'codigo', 'descripcion', 'latitud', 'longitud', 'logo', 'imagen', 'telefono', 'email', 'created_at', 'updated_at'];

}
