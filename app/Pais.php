<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pais';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idpais';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'created_at', 'updated_at'];

}
