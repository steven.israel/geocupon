<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Comercio;
use App\Categoria;

use Image;

class ComercioController extends Controller
{
     protected function random_string()
    {
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );

        for($i=0; $i<10; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editar     = false;
        $datos      = Comercio::all();

        return view('/Comercio/comercio')->with('datos', $datos)->with('editar', $editar);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/comercio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'        => 'required|string|max:255',
            'descripcion'   => 'required|string|max:1000',
            'lat'           => 'required',
            'lon'           => 'required',
            'telefono'      => 'required|string|min:1|max:8',
            'email'         => 'required|email|unique:usuario,email',
            'icono'         => 'required',
            'img'           => 'required',
            'codigo'        => 'required',
        ]);

        $ruta                   = public_path('/images/Images_comercio/');
        $ruta2                  = public_path('/images/Logo_comercio/');

        $imagenOriginal          = $request->file('icono');
        $imagenOriginal2         = $request->file('img');

            // crear instancia de imagen
        $imagen     = Image::make($imagenOriginal);
        $imagen2    = Image::make($imagenOriginal2);

            // generar un nombre aleatorio para la imagen
        $temp_name      = $this->random_string() . '.' . $imagenOriginal->getClientOriginalExtension();
        $temp_name2     = $this->random_string() . '.' . $imagenOriginal2->getClientOriginalExtension();

        $imagen->resize(300,300);
        $imagen2->resize(300,300);

            // guardar imagen
            // save( [ruta], [calidad])
        $imagen->save($ruta2 . $temp_name, 100);
        $imagen2->save($ruta . $temp_name2, 100);

        $comercio              = new Comercio();
        $comercio->codigo      = $request->codigo;
        $comercio->titulo      = $request->titulo;
        $comercio->descripcion = $request->descripcion;
        $comercio->latitud     = $request->lat;
        $comercio->longitud    = $request->lon;
        $comercio->imagen      = $temp_name2;
        $comercio->logo        = $temp_name;
        $comercio->email       = $request->email;
        $comercio->telefono    = $request->telefono;
        $comercio->save();

        $request->session()->flash('alert-info', 'Comercio Registrado');
        return redirect('/comercio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editar     = true;
        $comercio  = Comercio::find($id);
        $categorias = Categoria::all( ['idcategoria','titulo'] );

        return view('/Comercio/comercio')->with('comercio', $comercio)->with('editar', $editar)->with('categorias', $categorias);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator   = Validator::make($request->all(), [
            'titulo'        => 'required|string|max:255',
            'descripcion'   => 'required|string|max:1000',
            'lat'           => 'required',
            'lon'           => 'required',
            'telefono'      => 'required|string|min:1|max:8',
            'email'         => 'required|email',
            'icono'         => 'required',
            'img'           => 'required',
            'codigo'        => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/editar.comercio/'. $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $ruta                   = public_path('/images/Images_comercio/');
        $ruta2                  = public_path('/images/Logo_comercio/');

        $imagenOriginal         = $request->file('icono');
        $imagenOriginal2         = $request->file('img');

            // crear instancia de imagen
        $imagen  = Image::make($imagenOriginal);
        $imagen2  = Image::make($imagenOriginal2);

            // generar un nombre aleatorio para la imagen
        $temp_name  = substr(strtoupper(sha1(time())), 0, 6) . '.' . $imagenOriginal->getClientOriginalExtension();
        $temp_name2  = substr(strtoupper(sha1(time())), 0, 6) . '.' . $imagenOriginal2->getClientOriginalExtension();

        $imagen->resize(300,300);
        $imagen2->resize(300,300);

            // guardar imagen
            // save( [ruta], [calidad])
        $imagen->save($ruta2 . $temp_name, 100);
        $imagen2->save($ruta . $temp_name2, 100);

        $comercio              = Comercio::find($id);
        $comercio->codigo      = $request->codigo;
        $comercio->titulo      = $request->titulo;
        $comercio->descripcion = $request->descripcion;
        $comercio->latitud     = $request->lat;
        $comercio->longitud    = $request->lon;
        $comercio->imagen      = $temp_name2;
        $comercio->logo        = $temp_name;
        $comercio->email       = $request->email;
        $comercio->telefono    = $request->telefono;
        $comercio->save();

        $request->session()->flash('alert-info', 'Comercio Actualizado');
        return redirect('/comercio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        Comercio::destroy($id);

        $request->session()->flash('alert-info', 'Comercio Eliminado');
        return redirect('/comercio');
    }
}
