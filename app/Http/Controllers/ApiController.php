<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Usuario;
use App\Categoria;
use App\Comercio;
use App\Cupones;
use App\CuponUsuario;

class ApiController extends Controller
{


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * registro de usuarios
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Registro(Request $request)
    {
        $validator   = Validator::make($request->all(), [
            'email' => 'required|email|unique:usuario,email',
        ]);

        if ($validator->fails()) {

            return '{
                "errorCode": 0,
                "registro": "Ya existe el correo electronico" 
            }';

        }else{

            $usuario                       = new Usuario();
            $usuario->email                = $request->email;
            $usuario->nombre               = $request->nombre;
            $usuario->apellido             = $request->apellido;
            $usuario->pushtoken            = $request->pushtoken;
            $usuario->pais                 = $request->pais;
            $usuario->foto                 = $request->foto;
            $usuario->token_fb             = $request->token_fb;
            $usuario->fecha_nac            = $request->fecha_nac;
            $usuario->password             = $request->password;
            $usuario->idpais               = $request->idpais;
            $usuario->remember_token       = str_random(10);

            if ($usuario->save()) {

                $usuario = Usuario::find($usuario->idusuario);

                return '{
                    "errorCode": 1,
                    "registro": '.$usuario.' 
                }';
            }   
        }
    }

    /**
     * get lista categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getcategorias()
    {
        $categorias = Categoria::all();

        return '{
            "errorCode": 1,
            "categorias": '.$categorias.' 
        }';
    }

    /**
     * get listado de ofertas por categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getofertaXcategoria($id)
    {
        $cupones = Categoria::all()->where('idcategoria', $id);

        return '{
            "errorCode": 1,
            "cupones": '.$cupones.' 
        }';
    }

     /**
     * get listado de cupones
     *
     * @return \Illuminate\Http\Response
     */
    public function getcupones()
    {
        $cupones = Cupones::all();

        return '{
            "errorCode": 1,
            "cupones": '.$cupones.' 
        }';
    }

    /**
     * get listado de ofertas por categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getCuponesXvista()
    {
        $cupones = Cupones::all()->sortByDesc('visto');

        return '{
            "errorCode": 1,
            "cupones": '.$cupones.' 
        }';
    }

    /**
     * get listado ultimas de ofertas
     *
     * @return \Illuminate\Http\Response
     */
    public function getUltimosCupones()
    {
        $cupones = Cupones::all()->sortByDesc('fecha_creacion');

        return '{
            "errorCode": 1,
            "cupones": '.$cupones.' 
        }';
    }

    /**
     * registro de cupones
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RegistroCupon(Request $request)
    {

        $cupon                       = new Cupones();
        $cupon->idcomercio           = $request->idcomercio;
        $cupon->idcategoria          = $request->idcategoria;
        $cupon->titulo               = $request->titulo;
        $cupon->descripcion          = $request->descripcion;
        $cupon->imagen               = $request->img;
        $cupon->fecha_creacion       = $request->fecha_crea;
        $cupon->fecha_vencimiento    = $request->fecha_ven;
        $cupon->codigo               = $request->codigo;
        $cupon->visto                = 0;
        $cupon->redimido             = "0";
        $cupon->limite               = $request->limite;

        if ($cupon->save()) {

            return '{
                "errorCode": 1,
                "registro": "Cupon Registrado" 
            }';
        }else{
            return '{
                "errorCode": 0,
                "registro": "Cupon no registrado" 
            }';
        }   
    }

    /**
     * redimir cupon
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RedimirCupon(Request $request)
    {

        $redimido = CuponUsuario::all()->where('idcupon', $request->idcupon)->where('idusuario', $request->idusuario)->count();

        if ( $redimido > 0 ) {

            return '{
                "errorCode": 0,
                "registro": "El Cupon ya fue redimido" 
            }';

        }else{

            $cupon                  = Cupones::find($request->idcupon);
            $cupon->visto           = 2;

            if ($cupon->save()) {

                $redimir = $this->canjearCupon($cupon->idcupon, $request->idusuario);

                if ($redimir) {
                    
                    return '{
                        "errorCode": 1,
                        "registro": "Cupon Redimido" 
                    }';

                }else{

                    return '{
                        "errorCode": 3,
                        "registro": "Cupon no redmido" 
                    }';

                }
            }else{

                return '{
                    "errorCode": 2,
                    "registro": "No se actualizo la tabla cupones" 
                }';

            }   
        }       
    }

    public function canjearCupon($idcupon,$idusuario){

        $redimir             = new CuponUsuario();
        $redimir->idcupon    = $idcupon;
        $redimir->idusuario  = $idusuario;

        if ($redimir->save()) {
            return true;
        }

        return false;
    }

     /**
     * get listado de cupones redimidos por usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function getcuponesRedimidosXuser($id)
    {
        $cupones = CuponUsuario::all()->where('idusuario', $id);

        return '{
            "errorCode": 1,
            "cupones": '.$cupones.' 
        }';
    }

}
