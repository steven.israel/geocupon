<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\sendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Cupones;

class MailController extends Controller
{
    public function send()
    {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Titulo';
        $objDemo->demo_two = 'Titulo';
        $objDemo->sender = 'Nombre de usuario o correo';
        $objDemo->receiver = 'Nombre del usuario o correo';
 
        Mail::to("steven209701@hotmail.com")->send(new sendMail($objDemo));
    }
}
