<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;

use App\Cupones;
use App\Comercio;
use App\Categoria;

class CuponesController extends Controller
{

    protected function random_string()
    {
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );

        for($i=0; $i<10; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editar     = false;
        $datos      = Cupones::all();
        $comercio   = Comercio::all( ['idcomercio','titulo'] );
        $categoria  = Categoria::all( ['idcategoria','titulo'] );

        return view('/Cupones/cupones')->with('datos', $datos)->with('editar', $editar)->with('comercio', $comercio)->with('categoria', $categoria);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cupon($id)
    {
        $cupones   = Cupones::all()->where('idcategoria', $id);
        $categoria = Categoria::find($id); 

        return view('/Cupones/vistaCupones')->with('cupones', $cupones)->with('categoria', $categoria->titulo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/cupones');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'        => 'required|string|max:255',
            'descripcion'   => 'required|string|max:1000',
            'limite'        => 'required',
            'fecha_crea'    => 'required',
            'fecha_ven'     => 'required',
            'img'           => 'required',
            'comercio'      => 'required|min:1',
            'categoria'     => 'required|min:1',
            'codigo'        => 'required',
        ]);

        $ruta                   = public_path('/images/Images_cupones/');

        $imagenOriginal          = $request->file('img');

            // crear instancia de imagen
        $imagen     = Image::make($imagenOriginal);

            // generar un nombre aleatorio para la imagen
        $temp_name      = $this->random_string() . '.' . $imagenOriginal->getClientOriginalExtension();

        $imagen->resize(300,300);

            // guardar imagen
            // save( [ruta], [calidad])
        $imagen->save($ruta . $temp_name, 100);

        $cupones                       = new Cupones();
        $cupones->codigo               = $request->codigo;
        $cupones->titulo               = $request->titulo;
        $cupones->descripcion          = $request->descripcion;
        $cupones->limite               = $request->limite;
        $cupones->visto                = 0;
        $cupones->imagen               = $temp_name;
        $cupones->fecha_creacion       = $request->fecha_crea;
        $cupones->fecha_vencimiento    = $request->fecha_ven;
        $cupones->idcomercio           = $request->comercio;
        $cupones->idcategoria          = $request->categoria;
        $cupones->redimido             = 0;
        $cupones->save();

        $request->session()->flash('alert-info', 'Cupon Registrado');
        return redirect('/cupones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editar     = true;
        $cupon      = Cupones::all();
        $comercio   = Comercio::all( ['idcomercio','titulo'] );
        $categoria  = Categoria::all( ['idcategoria','titulo'] );

        return view('/Cupones/cupones')->with('cupon', $cupon)->with('editar', $editar)->with('comercio', $comercio)->with('categoria', $categoria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        Cupones::destroy($id);

        $request->session()->flash('alert-info', 'Cupon Eliminada');
        return redirect('/cupones');
    }
}
