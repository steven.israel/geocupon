<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('idusuario');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('email')->unique();
            $table->Integer('idpais')->unsigned();
            $table->string('pushtoken');
            $table->string('pais');
            $table->string('foto');
            $table->string('token_fb');
            $table->date('fecha_nac');
            $table->string('password');
            $table->String('remember_token');
            $table->timestamps();

            //llave de pais
            $table->foreign('idpais')->references('idpais')->on('Pais')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
