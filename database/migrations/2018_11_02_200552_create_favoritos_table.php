<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favoritos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idfavorito');
            $table->Integer('idusuario')->unsigned();
            $table->Integer('idcategoria')->unsigned();
            $table->timestamps();

            //llave de usuario y cupon
            $table->foreign('idusuario')->references('idusuario')->on('Usuario')->onDelete('cascade');
            $table->foreign('idcategoria')->references('idcategoria')->on('Categoria')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favoritos');
    }
}
