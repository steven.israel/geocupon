<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuponesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idcupon');
            $table->Integer('idcomercio')->unsigned();
            $table->Integer('idcategoria')->unsigned();
            $table->string('titulo');
            $table->string('descripcion');
            $table->string('imagen');
            $table->date('fecha_creacion');
            $table->string('fecha_vencimiento');
            $table->string('codigo');
            $table->string('limite');
            $table->string('visto');
            $table->string('redimido');
            $table->timestamps();

            //llave de comercio
            $table->foreign('idcomercio')->references('idcomercio')->on('Comercio')->onDelete('cascade');
            $table->foreign('idcategoria')->references('idcategoria')->on('Categoria')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupones');
    }
}
