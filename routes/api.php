<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//|-------------------------------------------------------------------------------------------------------------|
//|                                           RUTAS POST                                                        |
//|-------------------------------------------------------------------------------------------------------------|

//-----------------------------------------------------------------------------------(VALIDACION DE LOGIN)
Route::POST('/validarLogin', 'Auth\LoginController@validarLoginMovil');
//-----------------------------------------------------------------------------------(REGISTRO DE USUARIOS)
Route::POST('/registro', 'ApiController@registro');
//-----------------------------------------------------------------------------------(REGISTRO DE CUPONES)
Route::POST('/registroCupon', 'ApiController@RegistroCupon');
//-----------------------------------------------------------------------------------(REDIMIR CUPONES)
Route::POST('/redimirCupon', 'ApiController@RedimirCupon');

//|-------------------------------------------------------------------------------------------------------------|
//|                                           RUTAS GET                                                         |
//|-------------------------------------------------------------------------------------------------------------|

//-----------------------------------------------------------------------------------(GET LISTADO DE CATEGORIAS)
Route::GET('/categorias', 'ApiController@getcategorias');
//-----------------------------------------------------------------------------------(GET LISTADO DE CUPONES)
Route::GET('/ofertas/{id}', 'ApiController@getofertaXcategoria');
//-----------------------------------------------------------------------------------(GET LISTADO CUPONES POR CANTIDAD DE VISTAS)
Route::GET('/getCuponesXvista', 'ApiController@getCuponesXvista');
//-----------------------------------------------------------------------------------(GET LISTADO DE ULTIMOS CUPONES)
Route::GET('/getUltimosCupones', 'ApiController@getUltimosCupones');
//-----------------------------------------------------------------------------------(GET LISTADO DE CUPONES)
Route::GET('/getcupones', 'ApiController@getcupones');
//-----------------------------------------------------------------------------------(GET LISTADO DE CUPONES POR USUARIO)
Route::GET('/cuponesUser/{id}', 'ApiController@getcuponesRedimidosXuser');

