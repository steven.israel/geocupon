<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//-----------------------------------------------------------------------------------------------------------( CRUD LOGIN )
Route::get('/', function () { return view('Login/login'); });
Route::post('/validarLogin', 'Auth\LoginController@validarLogin');
Route::get('/logout', 'Auth\LoginController@Logout');

Route::get('/dashboard', 'DashboardController@index');

//-----------------------------------------------------------------------------------------------------------( CRUD CATEGORIAS )
Route::get('/categoria', 'CategoriaController@index');													//Listar
Route::post('/crear.categoria', 'CategoriaController@store');											//Crear
Route::get('/eliminar.categoria/{id}', 'CategoriaController@delete');									//Eliminar	
Route::get('/editar.categoria/{id}', 'CategoriaController@edit');										//Editar	
Route::post('/actualizar.categoria/{id}', 'CategoriaController@update');								//Actualizar
//-----------------------------------------------------------------------------------------------------------( CRUD COMERCIOS )
Route::get('/comercio', 'ComercioController@index');													//Listar
Route::post('/crear.comercio', 'ComercioController@store');												//Crear
Route::get('/eliminar.comercio/{id}', 'ComercioController@delete');										//Eliminar	
Route::get('/editar.comercio/{id}', 'ComercioController@edit');											//Editar	
Route::post('/actualizar.comercio/{id}', 'ComercioController@update');									//Actualizar
//-----------------------------------------------------------------------------------------------------------( CRUD CUPONES )
Route::get('/cupones', 'CuponesController@index');														//Listar
Route::post('/crear.cupon', 'CuponesController@store');													//Crear
Route::get('/eliminar.cupon/{id}', 'CuponesController@delete');											//Eliminar	
Route::get('/editar.cupon/{id}', 'CuponesController@edit');												//Editar	
Route::post('/actualizar.cupon/{id}', 'CuponesController@update');										//Actualizar
Route::get('/cupon/{id}', 'CuponesController@cupon');														//vista de cupones

Route::get('mail/send', 'MailController@send');
