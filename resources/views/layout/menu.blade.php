
<!-- BEGIN MENUBAR-->
<div id="menubar" class=" ">

	<div class="menubar-scroll-panel">

		<!-- BEGIN MAIN MENU -->
		<ul id="main-menu" class="gui-controls">

			<!-- BEGIN DASHBOARD -->
			<li>
				<a href="{{ url('/dashboard') }}" class="active">
					<div class="gui-icon"><i class="md md-home"></i></div>
					<span class="title">Home</span>
				</a>
			</li><!--end /menu-li -->
			<!-- END DASHBOARD -->

						<!-- BEGIN FORMS -->
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><span class="md md-settings"></span></div>
								<span class="title">Mantenimientos</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{ url('/categoria') }}" ><span class="title">Categoría</span></a></li>
								<li><a href="{{ url('/comercio') }}" ><span class="title">Comercios</span></a></li>
								<li><a href="{{ url('/cupones') }}" ><span class="title">Cupones</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						<!-- END FORMS -->
						<!-- <li>
							<a href="{{ url('/resportes') }}" >
								<div class="gui-icon"><i class="md md-assessment"></i></div>
								<span class="title">Reportes</span>
							</a>
						</li> --><!--end /menu-li -->
						<!-- END CHARTS -->

						

					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; </span> <strong>GEOCUPON</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->
