		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.css')}} " />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/materialadmin.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/material-design-iconic-font.min.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/rickshaw/rickshaw.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/morris/morris.core.css') }}" />
		<!-- licks para tablas -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/DataTables/jquery.dataTables.css?1423553989') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/DataTables/extensions/dataTables.colVis.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/DataTables/extensions/dataTables.tableTools.css') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/libs/wizard/wizard.css?1425466601') }}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css')}}" />
		<!-- END STYLESHEETS -->